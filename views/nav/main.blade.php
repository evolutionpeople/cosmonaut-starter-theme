<nav>
    <ul class="c-nav">
        @foreach($menu_items as $menu_item)
            <li class="{{implode(' ',$menu_item->classes)}}">
                <a href="{{$menu_item->url}}" title="{{$menu_item->name}}">{{$menu_item->name}}</a>
                @if($menu_item->has_children)
                    <ul>
                        @foreach($menu_item->children as $child)
                            <li class="{{implode(' ',$child->classes)}}">
                                <a href="{{$child->url}}" class="c-nav__link" title="{{$child->name}}">{{$child->name}}</a>
                            </li>
                        @endforeach
                    </ul>
                @endif
            </li>
        @endforeach
    </ul>
</nav>