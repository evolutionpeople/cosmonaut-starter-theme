<?php
get_header();

$post_new = new \Cosmonaut\Model\Post($post);
$post_new->withDetails();


wp_nav_menu(array(
    'menu' => 'main-nav',
    'walker' => new Cosmonaut\Theme\Navigation\MenuWalker(),
    'container' => ''
));

global $blade;
echo $blade->view('test',['post'=>$post_new]);

get_footer();
?>