<?php wp_footer(); ?>
<?php if(\Cosmonaut\Theme\Settings\Environment::environment() == 'staging'): ?>
    <script id="__bs_script__">//<![CDATA[
        document.write("<script async src='http://HOST:3000/browser-sync/browser-sync-client.js?v=2.18.2'><\/script>".replace("HOST", location.hostname));
        //]]></script>

<?php endif; ?>
</body>
</html>
