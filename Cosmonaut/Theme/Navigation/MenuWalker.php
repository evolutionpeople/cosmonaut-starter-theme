<?php


namespace Cosmonaut\Theme\Navigation;


class MenuWalker extends \Walker_Nav_Menu {

    function start_el(&$output, $item, $depth = 0, $args = [], $id = 0)
    {

        $prepend = '';
        $append = '';

        $indent = ($depth) ? str_repeat("\t", $depth) : '';

        $class_names = $value = '';

        $classes = empty($item->classes) ? [] : (array)$item->classes;

        $matches = preg_grep('/fa-/i', $classes);
        if ($matches)
        {
            $classes = array_diff($classes, $matches);
            $prepend = '<i class="fa ' . $matches[0] . '"></i>';
        }


        $class_names = join(' ', apply_filters('nav_menu_css_class', array_filter($classes), $item));


        $class_names = ' class="' . esc_attr($class_names) . '"';


        $output .= $indent . '<li' . $value . $class_names . '>';

        $attributes = !empty($item->title) ? ' title="' . esc_attr($item->title) . '"' : '';
        $attributes .= !empty($item->target) ? ' target="' . esc_attr($item->target) . '"' : '';
        $attributes .= !empty($item->xfn) ? ' rel="' . esc_attr($item->xfn) . '"' : '';
        $attributes .= !empty($item->url) ? ' href="' . esc_attr($item->url) . '"' : '';
        $attributes .= ' class="c-menu-item__link" ';


        $description = !empty($item->description) ? '<span>' . esc_attr($item->description) . '</span>' : '';

        if ($depth != 0)
        {
            //$description = $append = $prepend = "";
        }

        if (preg_match('/menu-item-has-children/', $class_names))
        {
            //$append .= '<i class="fa fa-arrow-circle-down" aria-hidden="true"></i>';
        }


        /*if($item->type == 'custom' && in_array($item->post_name,['search']))
        {
            $prepend = '<i class="fa fa-search"></i>';
            $menu_item_content = $args->link_before .$prepend.$append;
        }elseif($args->menu == 'social-menu'){
            //d($prepend);
            $menu_item_content = $args->link_before .$prepend.$append;
        }else{
            $menu_item_content = $args->link_before .$prepend.apply_filters( 'the_title', $item->title, $item->ID ).$append;
            $menu_item_content .= $description.$args->link_after;
        }*/

        $menu_item_content = $args->link_before . $prepend . apply_filters('the_title', $item->title, $item->ID) . $append;
        $menu_item_content .= $description . $args->link_after;


        $item_output = $args->before;
        $item_output .= '<a' . $attributes . '>';
        $item_output .= $menu_item_content;
        $item_output .= '</a>';
        $item_output .= $args->after;

        $output .= apply_filters('walker_nav_menu_start_el', $item_output, $item, $depth, $args);
    }

    function start_lvl(&$output, $depth = 0, $args = [])
    {
        parent::start_lvl($output, $depth, $args);
        $output = preg_replace('/sub-menu/', 'c-menu-item__submenu', $output);
    }
}