/*jslint node: true */
"use strict";

var $ = require('gulp-load-plugins')();
var argv = require('yargs').argv;
var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var merge = require('merge-stream');
var sequence = require('run-sequence');
var colors = require('colors');
var dateFormat = require('dateformat');
var del = require('del');
var cleanCSS = require('gulp-clean-css');

// Enter URL of your local server here
// Example: 'http://localwebsite.dev'
var URL = '';

// Check for --production flag
var isProduction = !!(argv.production);

// Browsers to target when prefixing CSS.
var COMPATIBILITY = [
    'last 2 versions',
    'ie >= 9',
    'Android >= 2.3'
];

var COMPONENT_DIRECTORY = 'bower_components';

// File paths to various assets are defined here.
var PATHS = {
    sass: [
        COMPONENT_DIRECTORY + '/foundation-sites/scss',
        COMPONENT_DIRECTORY + '/fontawesome/scss',
    ],
    javascript: [
        COMPONENT_DIRECTORY + '/foundation-sites/js/foundation.core.js',
        COMPONENT_DIRECTORY + '/foundation-sites/js/foundation.util.*.js',

        // Paths to individual JS components defined below
        // COMPONENT_DIRECTORY+'/foundation-sites/js/foundation.abide.js',
        // COMPONENT_DIRECTORY+'/foundation-sites/js/foundation.accordion.js',
        // COMPONENT_DIRECTORY+'/foundation-sites/js/foundation.accordionMenu.js',
        // COMPONENT_DIRECTORY+'/foundation-sites/js/foundation.drilldown.js',
        // COMPONENT_DIRECTORY+'/foundation-sites/js/foundation.dropdown.js',
        // COMPONENT_DIRECTORY+'/foundation-sites/js/foundation.dropdownMenu.js',
        // COMPONENT_DIRECTORY+'/foundation-sites/js/foundation.equalizer.js',
        // COMPONENT_DIRECTORY+'/foundation-sites/js/foundation.interchange.js',
        // COMPONENT_DIRECTORY+'/foundation-sites/js/foundation.magellan.js',
        // COMPONENT_DIRECTORY+'/foundation-sites/js/foundation.offcanvas.js',
        // COMPONENT_DIRECTORY+'/foundation-sites/js/foundation.orbit.js',
        // COMPONENT_DIRECTORY+'/foundation-sites/js/foundation.responsiveMenu.js',
        // COMPONENT_DIRECTORY+'/foundation-sites/js/foundation.responsiveToggle.js',
        // COMPONENT_DIRECTORY+'/foundation-sites/js/foundation.reveal.js',
        // COMPONENT_DIRECTORY+'/foundation-sites/js/foundation.slider.js',
        // COMPONENT_DIRECTORY+'/foundation-sites/js/foundation.sticky.js',
        // COMPONENT_DIRECTORY+'/foundation-sites/js/foundation.tabs.js',
        // COMPONENT_DIRECTORY+'/foundation-sites/js/foundation.toggler.js',
        // COMPONENT_DIRECTORY+'/foundation-sites/js/foundation.tooltip.js',

        // Include your own custom scripts (located in the custom folder)
        'source/javascript/custom/*.js',
    ],
    phpcs: [
        '**/*.php',
        '!wpcs',
        '!wpcs/**',
    ],
    pkg: [
        '**/**.*',
        'cache',
        '!node_modules/**/*',
        '!bower_components/**/*',
        '!source/**/*',
        '!packaged/**/*',
        '!scss/**/*',
        '!bower.json',
        '!composer.json',
        '!composer.lock',
        '!gulpfile.js',
        '!package.json',
        '!.DS_Store',
    ]
};

// Browsersync task
gulp.task('browser-sync', ['build'], function () {

    var files = [
        '**/*.php',
        'assets/images/**/*.{png,jpg,gif}',
    ];

    browserSync.init(files, {
        // Proxy address
        proxy: URL,

        // Port #
        // port: PORT
    });
});

// Compile Sass into CSS
// In production, the CSS is compressed
gulp.task('sass', function () {
    return gulp.src('source/scss/app.scss')
        .pipe($.sourcemaps.init())
        .pipe($.sass({
            includePaths: PATHS.sass
        }))
        .on('error', $.notify.onError({
            message: "<%= error.message %>",
            title: "Sass Error"
        }))
        .pipe($.autoprefixer({
            browsers: COMPATIBILITY
        }))
        // Minify CSS if run with --production flag
        .pipe($.if(isProduction, cleanCSS()))
        .pipe($.if(!isProduction, $.sourcemaps.write('.')))
        .pipe(gulp.dest('assets/css'))
        .pipe(browserSync.stream({match: '**/*.css'}));
});

// Lint all JS files in custom directory
gulp.task('lint', function () {
    return gulp.src('source/javascript/custom/*.js')
        .pipe($.jshint())
        .pipe($.notify(function (file) {
            if (file.jshint.success) {
                return false;
            }

            var errors = file.jshint.results.map(function (data) {
                if (data.error) {
                    return "(" + data.error.line + ':' + data.error.character + ') ' + data.error.reason;
                }
            }).join("\n");
            return file.relative + " (" + file.jshint.results.length + " errors)\n" + errors;
        }));
});

// Combine JavaScript into one file
// In production, the file is minified
gulp.task('javascript', function () {
    var uglify = $.uglify()
        .on('error', $.notify.onError({
            message: "<%= error.message %>",
            title: "Uglify JS Error"
        }));

    return gulp.src(PATHS.javascript)
        .pipe($.sourcemaps.init())
        .pipe($.babel({
            presets: ['es2015']
        }))
        .pipe($.concat('app.js', {
            newLine: '\n;'
        }))
        .pipe($.if(isProduction, uglify))
        .pipe($.if(!isProduction, $.sourcemaps.write()))
        .pipe(gulp.dest('assets/js'))
        .pipe(browserSync.stream());
});

// Copy task
gulp.task('copy', function () {
    // Font Awesome
    var fontAwesome = gulp.src(COMPONENT_DIRECTORY + '/fontawesome/fonts/**/*.*')
        .pipe(gulp.dest('assets/fonts'));
    return merge(fontAwesome);
});

// Package task
gulp.task('package', ['build'], function () {
    var fs = require('fs');
    var time = dateFormat(new Date(), "yyyy-mm-dd_HH-MM");
    var pkg = JSON.parse(fs.readFileSync('./package.json'));
    var title = pkg.name + '_' + time + '.zip';

    return gulp.src(PATHS.pkg)
        .pipe($.zip(title))
        .pipe(gulp.dest('packaged'));
});

// Build task
// Runs copy then runs sass & javascript in parallel
gulp.task('build', ['clean'], function (done) {
    sequence('copy',
        ['sass', 'javascript', 'lint'],
        done);
});

// Clean task
gulp.task('clean', function (done) {
    sequence(['clean:javascript', 'clean:css'],
        done);
});

// Clean JS
gulp.task('clean:javascript', function () {
    return del([
        'assets/js/app.js'
    ]);
});

// Clean CSS
gulp.task('clean:css', function () {
    return del([
        'assets/css/app.css',
        'assets/css/app.css.map'
    ]);
});

// Default gulp task
// Run build task and watch for file changes
gulp.task('default', ['build', 'browser-sync'], function () {
    // Log file changes to console
    function logFileChange(event) {
        var fileName = require('path').relative(__dirname, event.path);
        console.log('[' + 'WATCH'.green + '] ' + fileName.magenta + ' was ' + event.type + ', running tasks...');
    }

    // Sass Watch
    gulp.watch(['source/scss/**/*.scss'], ['clean:css', 'sass'])
        .on('change', function (event) {
            logFileChange(event);
        });

    // JS Watch
    gulp.watch(['source/javascript/custom/**/*.js'], ['clean:javascript', 'javascript', 'lint'])
        .on('change', function (event) {
            logFileChange(event);
        });
});
