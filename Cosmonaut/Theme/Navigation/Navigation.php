<?php
/**
 * Created by PhpStorm.
 * User: francescocolonna
 * Date: 30/11/16
 * Time: 09:57
 */

namespace Cosmonaut\Theme\Navigation;


class Navigation {

    protected $replacer = [];

    function initialize()
    {
        register_nav_menus([
            'main-nav'   => 'Main nav',
            'footer-nav' => 'Footer Nav'
        ]);
        add_filter('nav_menu_css_class', [$this, 'menu_item_class']);

        $this->replacer = [
            [
                'old' => 'menu-item',
                'new' => 'c-menu-item'
            ],
            [
                'old' => 'current-menu-item',
                'new' => 'c-menu-item--current'
            ],
            [
                'old' => 'menu-item-has-children',
                'new' => 'c-menu-item--has-children'
            ],
            [
                'old' => 'menu-item(-.*)',
                'new' => 'c-menu-item-$1'
            ],
            [
                'old' => 'current_page_item|page-item(.*)',
                'new' => ''
            ]
        ];
    }

    function menu_item_class($classes)
    {

        foreach ($this->replacer as $replace)
        {
            $classes = preg_replace('/^' . $replace['old'] . '$/', $replace['new'], $classes);
        }
        return $classes;
    }

}