<?php

require_once 'vendor/autoload.php';
define('BLADE_VIEWS', __DIR__ . '/views');
define('BLADE_CACHE', __DIR__ . '/cache');
use TorMorten\View\Blade;

/**
 * @global $blade
 */
global $blade;
$blade = Blade::create();


/**
 * Autoload Method based on namespace class
 * @param $namespace
 *
 */
/*function cosmonaut_autoload($namespace)
{
    $splitname = explode('\\', $namespace);

    $filename = implode(DIRECTORY_SEPARATOR, $splitname);

    if (file_exists(__DIR__ . '/' . $filename . '.php'))
    {
        require_once __DIR__ . '/' . $filename . '.php';
    }
}

spl_autoload_register('cosmonaut_autoload');*/


/**
 * Initialize the Settings class
 * @return \Cosmonaut\Theme\Settings\Settings
 */
function cosmonaut_theme_settings()
{
    global $cosmonaut_theme_settings;
    if (!isset($cosmonaut_theme_settings))
    {
        $cosmonaut_theme_settings = new \Cosmonaut\Theme\Settings\Settings();
        $cosmonaut_theme_settings->initialize();
    }
    return $cosmonaut_theme_settings;
}

cosmonaut_theme_settings();

/**
 * Initialize the Navigation Class
 * @return \Cosmonaut\Theme\Navigation\Navigation
 */
function cosmonaut_navigation()
{
    global $cosmonaut_navigation;
    if (!isset($cosmonaut_navigation))
    {
        $cosmonaut_navigation = new \Cosmonaut\Theme\Navigation\Navigation();
        $cosmonaut_navigation->initialize();
    }
    return $cosmonaut_navigation;
}

cosmonaut_navigation();

/**
 * Initialize the Protocol class
 * @return \Cosmonaut\Theme\Settings\Protocol
 */
function cosmonaut_protocol()
{
    global $cosmonaut_protocol;
    if (!isset($cosmonaut_protocol))
    {
        $cosmonaut_protocol = new \Cosmonaut\Theme\Settings\Protocol();
        $cosmonaut_protocol->initialize();
    }
    return $cosmonaut_protocol;
}

cosmonaut_protocol();