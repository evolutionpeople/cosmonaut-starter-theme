<?php

namespace Cosmonaut\Theme\Settings;


class Environment
{
    public static function environment()
    {
        $environments = require_once 'config/environment.php';
        foreach ($environments as $environment => $range)
        {
            if(in_array($_SERVER['SERVER_ADDR'],$range))
            {
                return $environment;
            }
        }
    }
}