<?php
namespace Cosmonaut\Theme\Settings;

class Settings {

    public function initialize()
    {
        add_action('wp_enqueue_scripts', [$this, 'init'], 5);
        add_action('init', [$this, 'clean'], 5);
        add_filter('style_loader_src', [$this, 'remove_version'], 9999);
        add_filter('script_loader_src', [$this, 'remove_version'], 9999);
        add_filter('the_generator', [$this, 'remove_rss_version']);
        add_filter('wp_head', [$this, 'recent_comments_style'], 1);
        add_action('wp_head', [$this, 'remove_recent_comments_style'], 1);
        add_theme_support('title-tag');
        add_theme_support('menus');
        add_theme_support( 'post-thumbnails' );
    }

    public function remove_rss_version()
    {
        return '';
    }

    public function recent_comments_style()
    {
        if (has_filter('wp_head', 'wp_widget_recent_comments_style'))
        {
            remove_filter('wp_head', 'wp_widget_recent_comments_style');
        }
    }

    public function remove_recent_comments_style()
    {
        global $wp_widget_factory;
        if (isset($wp_widget_factory->widgets['WP_Widget_Recent_Comments']))
        {
            remove_action('wp_head', [$wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style']);
        }
    }

    public function clean()
    {
        // EditURI link.
        remove_action('wp_head', 'rsd_link');

        // Category feed links.
        remove_action('wp_head', 'feed_links_extra', 3);

        // Post and comment feed links.
        remove_action('wp_head', 'feed_links', 2);

        // Windows Live Writer.
        remove_action('wp_head', 'wlwmanifest_link');

        // Index link.
        remove_action('wp_head', 'index_rel_link');

        // Previous link.
        remove_action('wp_head', 'parent_post_rel_link', 10);

        // Start link.
        remove_action('wp_head', 'start_post_rel_link', 10);

        // Canonical.
        remove_action('wp_head', 'rel_canonical', 10);

        // Shortlink.
        remove_action('wp_head', 'wp_shortlink_wp_head', 10);

        // Links for adjacent posts.
        remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10);

        // WP version.
        remove_action('wp_head', 'wp_generator');

        // Emoji detection script.
        remove_action('wp_head', 'print_emoji_detection_script', 7);

        // Emoji styles.
        remove_action('wp_print_styles', 'print_emoji_styles');
    }

    public function init()
    {
        $this->load_scripts();
        $this->load_styles();
    }

    public function remove_version($src)
    {
        return preg_replace('/\?ver=.+/', '', $src);
    }

    protected function load_scripts()
    {
        wp_deregister_script('jquery');
        wp_register_script('jquery', 'https://code.jquery.com/jquery-2.2.4.min.js', FALSE, NULL, TRUE);
        wp_enqueue_script('jquery');
        wp_enqueue_script('cosmonaut', get_template_directory_uri() . '/assets/js/app.js', ['jquery'], NULL, TRUE);
    }

    protected function load_styles()
    {
        wp_enqueue_style('main-stylesheet', get_template_directory_uri() . '/assets/css/app.css', [], FALSE, 'all');
    }
}